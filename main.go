package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/textract"
	ginadapter "github.com/awslabs/aws-lambda-go-api-proxy/gin"
	"github.com/gin-gonic/gin"
)

var initialized = false
var ginLambda *ginadapter.GinLambda
var buf bytes.Buffer

func main() {
	if true {
		lambda.Start(HandleRequest)
	} else {
		req := events.APIGatewayProxyRequest{
			Body: "",
		}
		res, err := HandleRequest(req)
		if err != nil {
			fmt.Print(err)
		} else {
			fmt.Print(res)
		}
	}
}

func HandleRequest(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	if !initialized {
		// stdout and stderr are sent to AWS CloudWatch Logs
		log.Printf("Gin cold start")
		router := gin.Default()

		// Path Methods
		router.GET("/", func(c *gin.Context) {
			// Redirect to Webpage
			c.Redirect(http.StatusTemporaryRedirect, "https://sudomind.domnick.io/")
		})
		router.POST("/", func(c *gin.Context) {
			c.Header("Access-Control-Allow-Origin", "*")
			// Prepare Captcha check
			secret := os.Getenv("secret")
			token := c.Request.FormValue("recaptcha")
			uri := "https://www.google.com/recaptcha/api/siteverify"
			formData := url.Values{
				"secret":   {secret},
				"response": {token},
			}

			// Ask Google if it is ok
			resp, err := http.PostForm(uri, formData)
			if err != nil {
				c.Error(err)
			}
			var result map[string]interface{}
			json.NewDecoder(resp.Body).Decode(&result)
			fmt.Println(result)

			success := fmt.Sprint(result["success"])
			if success == "false" {
				c.Status(http.StatusUnauthorized)
			} else {
				// Uplaod single file
				_, header, _ := c.Request.FormFile("body")

				ses, err := session.NewSession(&aws.Config{
					Region: aws.String(os.Getenv("Region"))})
				if err != nil {
					c.Error(err)
				}
				file, err := header.Open()
				// Set Input Values
				// s3s := s3.New(ses)
				// s3Input := &s3.PutObjectInput{
				// 	Bucket: aws.String("sudomind.domnick.io"),
				// 	Key:    aws.String(header.Filename),
				// 	Body:   file,
				// }
				// s3s.PutObject(s3Input)

				txt := textract.New(ses)
				// Prepare Textract
				strs := []string{"TABLES"}
				// input := &textract.AnalyzeDocumentInput{
				// 	Document: &textract.Document{
				// 		S3Object: &textract.S3Object{
				// 			Bucket: aws.String(os.Getenv("Bucket")),
				// 			Name:   aws.String(req.Body),
				// 		},
				// 	},
				// 	FeatureTypes: aws.StringSlice(strs),
				// }

				buf := bytes.NewBuffer(nil)
				file.Seek(0, 0)
				io.Copy(buf, file)
				input := &textract.AnalyzeDocumentInput{
					Document: &textract.Document{
						Bytes: buf.Bytes(),
					},
					FeatureTypes: aws.StringSlice(strs),
				}
				// Analyze Document
				output, err := txt.AnalyzeDocument(input)
				if err != nil {
					c.Error(err)
				}
				// Delete File
				// delete := &s3.DeleteObjectInput{
				// 	Bucket: aws.String(os.Getenv("Bucket")),
				// 	Key:    aws.String(req.Body),
				// }
				// s3s.DeleteObject(delete)
				// Prepare Output
				c.JSON(http.StatusOK, MapSudoku(*output))
			}
		})
		// Lambda Provider
		ginLambda = ginadapter.New(router)
		initialized = true
	}

	// If no name is provided in the HTTP request body, throw an error
	return ginLambda.Proxy(req)
}

func MapSudoku(out textract.AnalyzeDocumentOutput) [9][9]int {
	table := [9][9]int{}
	table[0][0] = 0
	for index := 0; index < len(out.Blocks); index++ {
		b := out.Blocks[index]
		if *b.BlockType == "CELL" {
			x := *b.ColumnIndex - 1
			y := *b.RowIndex - 1
			if x <= 8 && y <= 8 {
				table[x][y] = 0
				if len(b.Relationships) == 1 {
					if len(b.Relationships[0].Ids) == 1 {
						num := FindWordFromId(out, *b.Relationships[0].Ids[0])
						table[x][y] = num
					}
				}
			}
		}
	}
	return table
}

func FindWordFromId(out textract.AnalyzeDocumentOutput, id string) int {
	for index := 0; index < len(out.Blocks); index++ {
		b := out.Blocks[index]
		if *b.BlockType == "WORD" {
			if *b.Id == id {
				i, err := strconv.Atoi(*b.Text)
				if err == nil && i < 10 {
					return i
				}
				return 0
			}
		}
	}
	return 0
}

func ReturnError(err error) events.APIGatewayProxyResponse {
	return events.APIGatewayProxyResponse{
		StatusCode:      204,
		Body:            err.Error(),
		IsBase64Encoded: false,
	}
}
